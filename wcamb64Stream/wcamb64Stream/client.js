var WebSocketClient = require("ws");

try {
    //
} catch (e) {
    console.error("stream to server web not connected")
}

var webServerIp = "47.91.72.40" //"127.0.0.1" // "192.168.43.155" //
var webServerPort = 9250;
var WebSocketWebServerURL = 'ws://' + webServerIp + ':' + webServerPort;

var wsc;

try {
    wsc = new WebSocketClient(WebSocketWebServerURL);
} catch (e) {
    console.log("unable to connect the server")
}

function sendToWebServer(data) {
    if (wsc.readyState == wsc.OPEN) {
        wsc.send(data);
    } else {
        console.log("serverWeb Disconected")

        setTimeout(function () {
            try {
                wsc = new WebSocketClient(WebSocketWebServerURL);
            } catch (e) {
                console.log("Webserver: reconnecting...");
            }
        }, 500);
    }
}

wsc.on('open', function open(wsc) {
    //sendToWebServer('{"actionType":"state","actionProvider":"OFF"}');
    //sendToWebServer(JSON.stringify({actionType:"state", actionProvider:"OFF"}));
    sendToWebServer(JSON.stringify({actionProvider: {actionType: "state", actionData: "ALARM"}, devices:{USENS:{id:"USENS"}}}));
});