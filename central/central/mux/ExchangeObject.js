exports.ObjectExchangeDeviceEvent = class ObjectExchangeDeviceEvent {
	constructor(id, timestamp, state) {
		this.id = id;
		this.timestamp = timestamp;
		this.state = state;
	}
};

exports.ObjectExchangeDeviceVideo = class ObjectExchangeDeviceVideo {
	constructor(videoRessourceType, videoRessourceURI) {
		this.videoRessourceType = videoRessourceType;
		this.videoRessouceURI = videoRessourceURI;
	}
};

exports.ObjectExchangeAction = class ObjectExchangeAction {
	constructor(actionType, actionData) {
		this.actionType = actionType;
		this.actionData = actionData;
	}
};

exports.ObjectExchangeDevice = class ObjectExchangeDevice {
	constructor(id, type, state) {
		this.id = id;
		this.type = type;
		this.state = state;
		this.log = [];
		this.eventProvider = [];
		this.videoProvider = null;
	}

	addVideo(objectExchangeDeviceVideo) {
		this.videoProvider = objectExchangeDeviceVideo;
	}

	addEvent(id, timestamp, state) {
		this.eventProvider.push(new ObjectExchangeDeviceEvent(id, timestamp, state));
	}

};

exports.ObjectExchange = class ObjectExchange {
	constructor() {
		this.log = [];
		this.devices = {};
		this.actionProperties= {};
	}

	addDevice(objectExchangeDevice) {
		this.devices[objectExchangeDevice.id] = objectExchangeDevice;
		return objectExchangeDevice;
	}
	
	setAction(objectExchangeAction) {
		this.actionProperties = objectExchangeAction;
		return objectExchangeAction;
	}
};
