var request = require("./ExchangeObject.js");
var exchangeFlowObject = new request.ObjectExchange();
var WebSocketClient = require("/Users/yanisbaour/Desktop/central/node_modules/ws");

// LAN Port for Raspi to communicate to Central
var localPort = 9253;

var wsstreamrasp;
var wsstreamweb;
var webServerIp = "10.40.128.20" //"127.0.0.1" //
var webServerPort = 9250;
var WebSocketWebServerURL = 'ws://' + webServerIp + ':' + webServerPort;

var clients = [];
var clientsIPs = [];
var wsc;

try {
	wsc = new WebSocketClient(WebSocketWebServerURL);
} catch (e) {
	console.log("Unable to connect to the MUX")
}

function sendToDeviceId(id, data) {
	if (clientsIPs[id] != null) {
		if (clientsIPs[id].readyState == clientsIPs[id].OPEN) {
			clientsIPs[id].send(JSON.stringify(data));
		} else {
			console.error("Module Websockets id: " + id + "is closed, Last trace : " + JSON.stringify(data));
		}
	} else {
		console.error("Module Websockets id: " + id + " Last trace : " + JSON.stringify(data));
	}
}

function sendToWebServer(data) {
	if (wsc.readyState == wsc.OPEN) {
		wsc.send(data);
	} else {
		data.push(data)
		console.log("WebServer Disconected")

		setTimeout(function () {
			try {
				wsc = new WebSocketClient(WebSocketWebServerURL);
			} catch (e) {
				console.log("WebServer Reconnecting...");
			}
		}, 500);
	}
}

console.log("Central Module is now launched");

try {

	wsc.on('open', function open() {
		console.log("Server MUX online");
		exchangeFlowObject.setAction(new request.ObjectExchangeAction("registerCC", null))
		sendToWebServer(JSON.stringify(exchangeFlowObject));
		exchangeFlowObject.actionProperties = []
		console.log("Registering Central Module on WebServer");

	});

	wsc.on("error", function error() {
		console.log("Server MUX is unreachable");
		setTimeout(function () {
			try {
				wsc = new WebSocketClient(WebSocketWebServerURL);
			} catch (e) {
				console.log("WebSocketClient: Reconnecting...");
			}
		}, 500);
	});


	wsc.on('message', function (data) {
		console.log(data);
		var parsedJson = JSON.parse(data);
		console.log(parsedJson);
		console.log((new Date()).toISOString() + '  ' + wsc._socket.remoteAddress + "  sent \"" + data + "\"");

		if (parsedJson.actionProperties != null) {
			action = parsedJson.actionProperties
			console.log(action)
			console.log(action.actionType)
			switch (action.actionType) {
				case "moverobot":
					for (var idDevice in parsedJson["devices"]) {
						device = parsedJson["devices"][idDevice]
						requestToRasp = {
							id: idDevice,
							moverobot: parsedJson.actionProperties.actionData
						}
						sendToDeviceId(idDevice, requestToRasp)
						console.log("sending move action " + data + "  to " + idDevice)
					}
					break;
				case "state":
					console.log("STATE COND");
					for (var idDevice in parsedJson["devices"]) {

						console.log(action.actionData);
						device = parsedJson["devices"][idDevice]
						requestToRasp = {
							id: idDevice,
							setState: action.actionData
						}
						sendToDeviceId(idDevice, requestToRasp)
						exchangeFlowObject.setAction(new request.ObjectExchangeAction("refresh", null))
						sendToWebServer(JSON.stringify(exchangeFlowObject))
						exchangeFlowObject.actionProperties = null;
					}
					break;

				case "listDevices":
					exchangeFlowObject.setAction(new request.ObjectExchangeAction("refresh", null))
					console.log("CC listDevices")
					console.log(exchangeFlowObject)
					sendToWebServer(JSON.stringify(exchangeFlowObject))
					exchangeFlowObject.actionProperties = null
					console.log("Sending list of device to WebServer");
					break;
				case "startStream":
					startstream(action.actionData)
					console.log("Streamming WebServer");
					break;
				case "stopStream":
					stopstream()
					break;
			}
		}

	});
} catch (exception) {
	console.error("Unable to connect  " + WebSocketWebServerURL + " : " + exception);
}

function handle_reco_rasp(clientidX) {

	if (exchangeFlowObject.devices[clientidX].state != "DECO") {
		exchangeFlowObject.setAction(new request.ObjectExchangeAction("networkAlert", {
			type: "RECO",
			path: "cc." + clientidX
		}))

		sendToWebServer(JSON.stringify(exchangeFlowObject));
		exchangeFlowObject.actionProperties = null


	}
}

function startstream(deviceId) {
	try {
		wsstreamrasp = new WebSocketClient("ws://192.168.43.212:"+8000+"/websocket")

		wsstreamweb = new WebSocketClient("ws://" + webServerIp + ":" + 1338)
	} catch (e) {
		console.log("cannot connect to rasp websocket : ")
	}
	var wsstreamraspOpen;
	var wsstreamwebOpen;
	console.log("webserver ask start stream function");
	wsstreamrasp.on('open', function open() {
		wsstreamraspOpen = true;
		wsstreamrasp.send("read_camera");
		console.log("stream to rasp opened")
	});
	wsstreamrasp.on('close', function close() {
		wsstreamraspOpen = false;
		console.log("stream to rasp closed")
	});
	wsstreamweb.on('open', function open() {
		wsstreamwebOpen = true;
		console.log("stream to webserver closed")

	});
	wsstreamweb.on('close', function close() {
		wsstreamwebOpen = false;
		console.log("stream to webserver closed")
	});

	wsstreamrasp.on("message", function (data) {
		if (wsstreamwebOpen == true && wsstreamweb.readyState == wsstreamweb.OPEN){
			wsstreamweb.send(data);
		}
			
	});


}

function stopstream() {
	wsstreamweb.close()
	wsstreamrasp.close()
}



/////////////////////////////////////////////////SERVER/////////////////////////////////////////////

process.title = 'node-cc';

var WebSocketServer = require("ws").Server;
var ws = new WebSocketServer({ port: localPort });
console.log("Server Arduino started...");

/**
* Helper function for escaping input strings
*/
function htmlEntities(str) {
	return String(str)
		.replace(/&/g, '&amp;').replace(/</g, '&lt;')
		.replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

ws.on('connection', function (ws) {

	console.log((new Date()).toISOString() + ' Connection from: '
		+ ws._socket.remoteAddress + '.');
	//Save index to remove user later
	var index = clients.push(ws) - 1;
	console.log((new Date()).toISOString() + ' Connection accepted.');

	// Disconnected
	ws.on('close', function (connection) {
		console.log((new Date()) + " Index number " + index + "  disconnected.");
		clients.splice(index, 1);
	});


	ws.on('message', function (data) {
		console.log("hey");
		var ob = JSON.parse(data)
		console.log((new Date()).toISOString() + " LAN member "
			+ ws._socket.remoteAddress + " sent \"" + data + "\"");

		// registering the device
		if (ob["id"] != undefined && ob["networkRequest"] != undefined) {
			// console.log("networkService  id : " + ob["id"] + " ans:" + ob["networkRequest"])
			return;

		} else if (ob["id"] != undefined && ob["state"] != undefined) {
			//If the data is not empty then save this object

			switch (ob["state"]) {
				// play now and callback when playend
				case "ALARM":
					console.log("ALARM WARNING !!!!!")
					break;
				case "DECO":
					exchangeFlowObject.setAction(new request.ObjectExchangeAction("networkAlert", {
						type: "DECO",
						path: "rasp." + ob["id"]
					}))
					sendToWebServer(JSON.stringify(exchangeFlowObject));
					exchangeFlowObject.actionProperties = null
					console.log("device id" + ob["id"] + "is disconnected")
					break;

				//READY, OFF
				default:

					if (exchangeFlowObject["devices"][ob["id"]] == undefined) {
						exchangeFlowObject.addDevice(new request.ObjectExchangeDevice(ob["id"], ob["type"], ob["state"]))
						clientsIPs[ob["id"]] = ws
						console.log("New device, now registering in Request Object: ");
						console.log(ob);
					} else {
						if (exchangeFlowObject["devices"][ob["id"]].state == "DECO") {
							clientsIPs[ob["id"]] = ws
							handle_reco_rasp(ob["id"]);
						}
						console.log("Device already existe,  State: " + exchangeFlowObject["devices"][ob["id"]].state + " -----> " + ob["state"]);
					}
					break;
			}
			exchangeFlowObject["devices"][ob["id"]].state = ob["state"];

		}

		try {
			exchangeFlowObject.setAction(new request.ObjectExchangeAction("refresh", null))
			sendToWebServer(JSON.stringify(exchangeFlowObject))
			exchangeFlowObject.actionProperties = []
		} catch (e) {
			console.log
		};
	});
});
