
// Lumiere allumee => alarme declenchee

int analogPin = 0;
int analogValue = 0;
const int green_led = 2;
const int red_led = 7;
const int buzzer = 9;

const char uid[6] = "APHOT1";

int etatled = 0;// varibale de la led, soit éteinte soit allumée
/**/

char command[1]; //incoming command
char state; //current state
int go_alarm; //should we trigger alarm ?

// For log
const String source ="photoduino";
String info_sup ="";
String myState;

void setup(){

  Serial.begin(9600);

  Serial.begin (9600);
  pinMode(green_led, OUTPUT);
  pinMode(red_led, OUTPUT);
  pinMode(buzzer, OUTPUT);
  
  state = 'r' ; // État armé au départ. (idle)
  Serial.println(" HELLO");
}

void manage_button() {
  //stockage de la position du bouton dans la variable etatbouton
  analogValue = analogRead(analogPin);
  //Serial.println (analogValue); 
    if (analogValue > 110) {//si etatbouton est différent de 1 (je rappelle que les états sont inversés dûs à la résistance de PULLUP  
      if (etatled){//et que si etatled est à 1  
        etatled = 0;//nous passons etatled à
        noTone(buzzer);  
      }else{  
        //Serial.println ("PRESS BOUTTON");
        etatled = 1;//sinon nous le passons à 1
        go_alarm = 1;
        }
    }
}

void manage_states() {

  command[0] = '\0' ;

  if(Serial.available()) {

    command[0]=Serial.read();

    if(command[0] == 'u') { // We're asked to provide the uid
      Serial.println(uid);
    } else if(command[0] == 's') { //We're asked the current state
      switch(state) {
          case 'r':
            myState = "READY";//READY
            Serial.println(myState);
            break;
          case 'a':
            myState = "ALARM";//ALARME
            Serial.println(myState);
            break;
          case 'd':
            myState = "DEAC";//OFF
            Serial.println(myState);
            break;
      }
    }

    Serial.flush();
  }

  if(state == 'r') {// ready

    digitalWrite(red_led, LOW);
    digitalWrite(green_led, HIGH);
    
    manage_button();
    noTone(buzzer);
    
    if(command[0] == 'a' || go_alarm) {
      state = 'a';
      myState = "ALARM";
      info_sup = analogValue;
      Serial.println(myState);
            

    } else if(command[0] == 'd') {
      state = 'd';
      myState = "DEAC";
      Serial.println(myState);


    } else if(command[0] == 'r') {
      myState = "READY";
      Serial.println(myState);

    }


   } else if(state == 'a') { // alarm 

      digitalWrite(green_led, LOW);
      digitalWrite(red_led, HIGH);
      
      tone(buzzer, 700);
      
      if(command[0] == 'r') {
        state = 'r';
        myState = "READY";
        Serial.println(myState);

      }
      else if(command[0] == 'd')
      {
        state = 'd' ;
        myState = "DEAC";
        Serial.println(myState);

      }
      else if(command[0] == 'a')
      {
        myState = "ALARM";
        info_sup = analogValue;
        Serial.println(myState);

      }


   } else if(state == 'd') { // deactivated
    
      digitalWrite(red_led, HIGH);
      digitalWrite(green_led, LOW);
      
      noTone(buzzer);
      go_alarm = 0;

      if(command[0] == 'r')
      {
        state = 'r' ;
        noTone(buzzer);
        myState = "READY";
        Serial.println(myState);

      }
      else if(command[0] == 'a')
      {
        state = 'a' ;
        myState = "ALARM";
        info_sup = analogValue;
        Serial.println(myState);

      }
      else if(command[0] == 'd')
      {
        myState = "DEAC";
        Serial.println(myState);

      }
    }
}

void loop()
{
  manage_states();
}
