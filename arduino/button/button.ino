
int bouton = 2;// bouton sur la PIN 2
const int green_led = 4;
const int red_led = 3;
const int buzzer = 9;

const char uid[6] = "ABUTT0";

int etatbouton = 0; // variable etat bouton (appuie ou non appuie
int dernieretatbouton = 0;// variable memoire derniere position du bouton
int etatled = 0;// varibale de la led, soit éteinte soit allumée
/**/

char command[1]; //incoming command
char state; //current state
int go_alarm; //should we trigger alarm ?
int sensor_measure;

// For log
const String source ="Sensor";
String info_sup ="";
String myState;

void setup(){

  Serial.begin(9600);

  Serial.begin (9600);
  pinMode(bouton, INPUT_PULLUP);
  pinMode(green_led, OUTPUT);
  pinMode(red_led, OUTPUT);
  pinMode(buzzer, OUTPUT);
  
  state = 'r' ; // État armé au départ. (idle)
  Serial.println(" HELLO");
}

void manage_button() {
  //stockage de la position du bouton dans la variable etatbouton
  etatbouton = digitalRead(bouton);
  //noTone(buzzer);  
  if (etatbouton != dernieretatbouton) {  
    if (!etatbouton) {//si etatbouton est différent de 1 (je rappelle que les états sont inversés dûs à la résistance de PULLUP  
      if (etatled){//et que si etatled est à 1  
        etatled = 0;//nous passons etatled à
        //Serial.println ("LED ALLUMEE"); 
        noTone(buzzer);  
      }else{  
        //Serial.println ("PRESS BOUTTON");
        etatled = 1;//sinon nous le passons à 1
        //Serial.println ("LED ALLUMEE");
        go_alarm = 1;
        //Serial.println ("ALERT ACT");
      }
    }
    dernieretatbouton = etatbouton;
  }
  //Serial.println (etatled);
}

void manage_states() {

  command[0] = '\0' ;

  if(Serial.available()) {

    command[0]=Serial.read();

    if(command[0] == 'u') { // We're asked to provide the uid
      Serial.println(uid);
    } else if(command[0] == 's') { //We're asked the current state
      switch(state) {
          case 'r':
            myState = "READY";
            Serial.println(myState);
            break;
          case 'a':
            myState = "ALARM";
            Serial.println(myState);
            break;
          case 'd':
            myState = "DEAC";
            Serial.println(myState);
            break;
      }
    }

    Serial.flush();
  }

  if(state == 'r') {// ready

    digitalWrite(red_led, LOW);
    digitalWrite(green_led, HIGH);
    
    manage_button();
    noTone(buzzer);
    
    if(command[0] == 'a' || go_alarm) {
      state = 'a';
      myState = "ALARM";
      Serial.println(myState);
            

    } else if(command[0] == 'd') {
      state = 'd';
      myState = "DEAC";
      Serial.println(myState);


    } else if(command[0] == 'r') {
      myState = "READY";
      Serial.println(myState);

    }


   } else if(state == 'a') { // alarm 

      digitalWrite(green_led, LOW);
      digitalWrite(red_led, HIGH);
      
      tone(buzzer, 500);
      
      if(command[0] == 'r') {
        state = 'r';
        myState = "READY";
        Serial.println(myState);

      }
      else if(command[0] == 'd')
      {
        state = 'd' ;
        myState = "DEAC";
        Serial.println(myState);

      }
      else if(command[0] == 'a')
      {
        myState = "ALARM";
        Serial.println(myState);

      }


   } else if(state == 'd') { // deactivated
    
      digitalWrite(red_led, HIGH);
      digitalWrite(green_led, LOW);
      
      noTone(buzzer);
      go_alarm = 0;

      if(command[0] == 'r')
      {
        state = 'r' ;
        noTone(buzzer);
        myState = "READY";
        Serial.println(myState);

      }
      else if(command[0] == 'a')
      {
        state = 'a' ;
        myState = "ALARM";
        Serial.println(myState);

      }
      else if(command[0] == 'd')
      {
        myState = "DEAC";
        Serial.println(myState);

      }
    }
}

void loop()
{
  manage_states();
}
